 # What is inside
- Boxbilling v4.22-beta.1
- Niagahoster Landing Page
#### Platform
- PHP 7.4
- MySQL 5.6
#### Container
- docker_mysql5.6 (Port: **3056**)
- docker_nginx7.4 (Port: **8004**)
- docker_php7.4_niagahoster
- docker_php7.4_boxbilling

#  How to Install
1. Extract **docker-niagahoster.zip** file
2. Enter the docker-niagahoster directory and execute ` ./rebuild` and wait till the docker finish its process
3. Execute `./boxbilling` to import the boxbilling data seed with admin credential: 

    Email : arifrahmadanivinanda@gmail.com 
	Password : rahasia123
    
4. Edit the computer hosts and add `127.0.0.1 niagahoster.local boxbilling.local` into the end of the file
5. Now you can use http://niagahoster.local:8004/ for landing page and http://boxbilling.local:8004/ for Boxbilling v4.22-beta.1

